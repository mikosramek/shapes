class Square{
  float x,y,size;
  PVector topLeft,topRight,botLeft,botRight;
  
  Square(int x, int y, int size){
    topLeft = new PVector(x - size/2, y - size/2);
    topRight = new PVector(x + size/2, y - size/2);
    botLeft = new PVector(x - size/2, y + size/2);
    botRight = new PVector(x + size/2, y + size/2);
    
    this.size = size;
  }
  
  public void DrawSquare(){
    ellipse(topLeft.x + x,topLeft.y + y, 3, 3);
    ellipse(topRight.x + x,topRight.y + y, 3, 3);
    ellipse(botRight.x + x,botRight.y + y, 3, 3);
    ellipse(botLeft.x + x,botLeft.y + y, 3, 3);
    
    stroke(255);
    
    line(topLeft.x+x,topLeft.y+y, topRight.x+x,topRight.y+y);
    line(topRight.x+x,topRight.y+y, botRight.x+x,botRight.y+y);
    line(botRight.x+x,botRight.y+y, botLeft.x+x,botLeft.y+y);
    line(botLeft.x+x,botLeft.y+y, topLeft.x+x,topLeft.y+y);
    
    UpdateSquarePosition();
  }
  public void UpdateSquarePosition(){
    x = GetMouse().x;
    y = GetMouse().y;
    
    
    
  
    
    topRight = RotatePoint(topRight);
    topLeft = RotatePoint(topLeft);
    botRight = RotatePoint(botRight);
    botLeft = RotatePoint(botLeft);
  }
  
  private PVector RotatePoint(PVector point){
    if(point.x + x >= x && point.y + y <= y){
      point.x++;
      point.y++;
    }
    if(point.x + x >= x && point.y + y >= y){
      point.x--;
      point.y++;
    }
    if(point.x + x <= x && point.y + y > y){
      point.x--;
      point.y--;
    }
    if(point.x + x <= x && point.y + y <= y){
      point.x++;
      point.y--;
    }
   
   return point;
  }
  
}