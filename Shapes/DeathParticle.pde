class DeathParticle{
  PVector start, end;
  float speed;
  PVector movementVector, gravity, startVelocity;
  float time;
  public DeathParticle(PVector a, PVector b){
    start = a;
    end = b;
    speed = globalSpeed;
    //print("\nNew Death!");
    time = 0.1;
    gravity = new PVector(0,6);
    startVelocity = new PVector(random(-5,5),-15 * speed);
    movementVector = new PVector(0,0);
  }
  
  private void DrawParticle(){
    stroke(255);
    fill(255);
    line(start.x,start.y,end.x,end.y);
    Fall();
  }
  
  private void Fall(){
    movementVector = movementVector.add((startVelocity.add(gravity)).div(time+=2));
    
    
    start = start.add(movementVector);
    end = end.add(movementVector);
    
    if((start.x > width && end.x > width || start.x < 0 && end.x < 0) && start.y > height && end.y > height){
      RemoveDeathParticle(this);
      //print("Removed");
    }
    
  }
  
}