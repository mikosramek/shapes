class Shape {

  float x, y, rotationSpeed;
  int size, sides;
  PVector[] points;
  PVector[] previewPoints;
  public boolean followingMouse;
  private boolean rotating;
  public float rotationDirection = 1;
  private float previewSize;
  private BoundingBox boundingBox;
  private Shape[] overlappingShapes;
  private boolean underInspection;
  private float startRotation;

  private boolean alive = true;

  //Creating a shape at (MouseX,MouseY)
  Shape(int size, int sides, float rotationSpeed) {
    this.size = size;
    this.sides = sides;
    this.rotationSpeed = rotationSpeed;
    points = new PVector[sides];
    previewPoints = new PVector[sides];
    previewSize = size / 2;
    followingMouse = true;

    boundingBox = new BoundingBox(size, x, y, this);
    rotating = true;


    points[0] = new PVector(1, size);
    previewPoints[0] = points[0].normalize(previewPoints[0]).mult(previewSize);
    //points[1] = new PVector(0, size);
    //points[2] = new PVector(0, size);

    //points[1].rotate(2*PI/sides);
    //points[2] = points[1].copy();
    //points[2].rotate(2*PI/sides);
    for (int index = 1; index < sides; index++) {
      PVector newPoint = points[index-1].copy();
      newPoint.rotate(2*PI/sides);
      points[index] = newPoint;
      previewPoints[index] = points[index].normalize(previewPoints[index]).mult(previewSize);
    }
  }

  //Creating a Shape at (x,y)
  Shape(int size, int sides, float rotationSpeed, int x, int y) {
    this.size = size;
    this.sides = sides;
    this.x = x;
    this.y = y;
    this.rotationSpeed = rotationSpeed;
    points = new PVector[sides];
    followingMouse = false;
    rotationDirection = 1;

    boundingBox = new BoundingBox(size, x, y, this);

    rotating = true;

    points[0] = new PVector(1, size);
    for (int index = 1; index < sides; index++) {
      PVector newPoint = points[index-1].copy();
      newPoint.rotate(2*PI/sides);
      points[index] = newPoint;
    }
  }

  public void DrawPreview(float pX, float pY) {
    for (int index = 0; index < points.length; index++) {
      stroke(0);
      ellipse(previewPoints[index].x + pX, previewPoints[index].y + pY, 3, 3);
    }
    for (int index = 0; index < points.length-1; index++) {
      stroke(255);
      line(previewPoints[index].x + pX, previewPoints[index].y + pY, previewPoints[index+1].x + pX, previewPoints[index+1].y + pY);
    }
    line(previewPoints[previewPoints.length-1].x + pX, previewPoints[previewPoints.length-1].y + pY, previewPoints[0].x + pX, previewPoints[0].y + pY);
  }

  public void DrawShape() {

    if (alive) {


      UpdateSpeed();
      //Rotate the point;
      if (rotating) {
        Rotate();
      }
      //Update the mouse reference point
      if (followingMouse) {
        UpdateMouseCoords();
      }
      //Draw points for the shape;
      for (int index = 0; index < points.length; index++) {
        //SHAPE POINT INDEX
        //text("#"+index, points[index].x + x, points[index].y + y);

        //line(x,y,points[index].x + x, points[index].y + y);
        stroke(0);
        ellipse(points[index].x + x, points[index].y + y, 3, 3);
      }

      //SHAPE SPEED
      //text("Speed: " + rotationSpeed, points[2].x + x, points[2].y+y);
      if (showDebug) {
        DrawDebugInfo();
      }


      //DrawInsideOfShape();
      //Draw the lines between the points
      for (int index = 0; index < points.length-1; index++) {
        stroke(255);
        line(points[index].x + x, points[index].y + y, points[index+1].x + x, points[index+1].y + y);
      }
      line(points[points.length-1].x + x, points[points.length-1].y + y, points[0].x + x, points[0].y + y);

      //FillShape();
      //DrawInsideOfShape();

      
    }
  }

  private void DrawDebugInfo() {
    text("Rotation: " + GetRotation(), points[0].x + x, points[0].y+y);
    text("Start Rotation: " + startRotation, points[0].x + x, points[0].y+y + 15);
    text("Sus AF: " + underInspection, points[1].x + x, points[1].y+y);
    for (int i = 0; i < points.length; i++) {
      text(i, points[i].x + x - 15, points[i].y + y);
    }
    boundingBox.DrawBoundingBox();
  }

  private void DrawInsideOfShape() {
    stroke(0);
    for (int index = 0; index < points.length-1; index++) {
      triangle(x, y, points[index].x+x, points[index].y+y, points[index+1].x+x, points[index+1].y+y);
    }
    triangle(x, y, points[0].x+x, points[0].y+y, points[points.length-1].x+x, points[points.length-1].y+y);
    stroke(0);
  }


  private void FillShape() {
    PVector sideVector = points[0].copy();
    float sideSize = sideVector.sub(points[1]).mag();
    float rotation = GetRotation();
    switch(sides) {
    case 3:
      PVector temp = points[1].copy();
      temp.sub(points[0]);
      PVector temp2 = points[2].copy();
      temp2.sub(points[0]);
      for (float n = 0; n < 1; n+=0.1f) {
        line(temp.x*n+points[0].x+x, temp.y*n+points[0].y+y, temp2.x*n+points[0].x+x, temp2.y*n+points[0].y+y);
      }
      break;    
    default:
      PVector[] tempVs = new PVector[sides-1];
      for (int n = sides-1; n > 0; n--) {
        //tempVs[n] = points[n].copy();
        //tempVs[n].sub(points[n-1]);
      }
      break;
    }
  }

  public void PlaceShape() {
    followingMouse = false;
    //rotating = false;

    //Check for one full rotation. If it doesn't hit during that time. Turn it off.
    overlappingShapes = boundingBox.CheckOverlap();
    if (overlappingShapes.length > 0) {
      underInspection = true;
    } else {
      underInspection = false;
    }
    startRotation = float(nf(GetRotation(), 2, 2));
  }

  /*
        float angle = atan(point.y/point.x);
   angle+= PI/180;
   point.x = size * cos(angle);
   point.y = size * sin(angle);
   
   */

  public void Rotate() {

    for (int index = 0; index < points.length; index++) {
      points[index] = RotatePoint(points[index]);
    }

    CheckCollision();
  }

  private void CheckCollision() {
    if (underInspection) {
      //At this point if the rotation is = to the starting rotation, then it has completed a full rotation. As it will rotate one step before checking
      if (startRotation == float(nf(GetRotation(), 2, 2))) {
        underInspection = false;
        boundingBox.WeGucci();
        if(lives <= 2){
          lives++; 
        }
      }
      for (int i = 0; i < sides - 1; i++) {
        PVector tempA1 = new PVector(points[i].x + x, points[i].y + y);
        PVector tempA2 = new PVector(points[i + 1].x + x, points[i + 1].y + y);
        for (int shapeIndex = 0; shapeIndex < overlappingShapes.length; shapeIndex++) {       
          for (int n = 0; n < overlappingShapes[shapeIndex].sides - 1; n++) {
            PVector tempB1 = new PVector(overlappingShapes[shapeIndex].points[n].x + overlappingShapes[shapeIndex].x, overlappingShapes[shapeIndex].points[n].y + overlappingShapes[shapeIndex].y);
            PVector tempB2 = new PVector(overlappingShapes[shapeIndex].points[n + 1].x + overlappingShapes[shapeIndex].x, overlappingShapes[shapeIndex].points[n + 1].y + overlappingShapes[shapeIndex].y);
            if (Util.DetectLineCollision(tempA1, tempA2, tempB1, tempB2)) {
              overlappingShapes[shapeIndex].Kill();
              HitShape();
            }
          }
        }
      }
    }
  }

  private float GetRotation() {
    float angle = atan(points[0].y/points[0].x);
    return angle;
  }
  private PVector RotatePoint(PVector point) {
    if (point.x + x >= x && point.y + y <= y) {
      float angle = atan(point.y/point.x);
      angle+= PI/180*rotationSpeed * rotationDirection;
      point.x = size * cos(angle);
      point.y = size * sin(angle);
    }
    if (point.x + x > x && point.y + y >= y) {
      float angle = atan(point.y/point.x);
      angle+= PI/180*rotationSpeed * rotationDirection;
      point.x = size * cos(angle);
      point.y = size * sin(angle);
    }
    if (point.x + x <= x && point.y + y > y) {
      float angle = atan(point.y/point.x);
      angle+= PI/180*rotationSpeed * rotationDirection;
      point.x = size * cos(angle+PI);
      point.y = size * sin(angle+PI);
    }
    if (point.x + x <= x && point.y + y <= y) {
      float angle = atan(point.y/point.x);
      angle+= PI/180*rotationSpeed * rotationDirection;
      point.x = size * cos(angle+PI);
      point.y = size * sin(angle+PI);
    }

    return point;
  }

  private void UpdateMouseCoords() {
    x = GetMouse().x;
    y = GetMouse().y;
    boundingBox.UpdateBoundingBox(x, y);
  }


  public void PrintPoints() {
    for (int index = 0; index < points.length; index++) {
      print(points[index].x + ", " + points[index].y +"\n");
    }
  }

  private void UpdateSpeed() {
    rotationSpeed = Shapes.GetSpeed();
  }

  private void HitShape() {
    //print("Hit a shape!");
    //Lose A Life
    lives--;
    Kill();
  }
  private void Kill() {
    if (alive) {
      //Instantiate Death Particles
      for (int i = 0; i < sides - 1; i++) {
        PVector tempA1 = new PVector(points[i].x + x, points[i].y + y);
        PVector tempA2 = new PVector(points[i + 1].x + x, points[i + 1].y + y);
        AddDeathParticle(new DeathParticle(tempA1, tempA2));
      }
      PVector tempA1 = new PVector(points[sides-1].x + x, points[sides-1].y + y);
      PVector tempA2 = new PVector(points[0].x + x, points[0].y + y);
      AddDeathParticle(new DeathParticle(tempA1, tempA2));     //Destroy it!
      alive = false;
    }
    //print("\nKilled!");
  }
}