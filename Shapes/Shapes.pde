


Shape firstShape;

Shape currentShape;
Shape nextShape;

ArrayList<Shape> existingShapes;
ArrayList<DeathParticle> deathParticles;
public static float globalSpeed;
public static boolean showDebug = false;
float speedIncrease;
int score, level, lives, demiLives;
float UIBuffer, UIYValue;
float minShapeSize, maxShapeSize;
PImage heart;
void setup() {
  size(1000, 800);
  noCursor();
  globalSpeed = 0.95f;
  speedIncrease = 0.05f;
  level = 1;
  score = 0;
  UIBuffer = 40;
  UIYValue = 75;
  minShapeSize = 25;
  maxShapeSize = 75;
  lives = 3;
  demiLives = 3;
  //firstShape = new Shape(40, 3, globalSpeed);
  existingShapes = new ArrayList<Shape>();
  deathParticles = new ArrayList<DeathParticle>();
  SpawnLevelShapes();
  nextShape = new Shape((int)random(minShapeSize, maxShapeSize), (int)random(3, 4+level), globalSpeed);  
  AddShape();
  //existingShapes.add(currentShape);
  //currentShape = firstShape;
  heart = loadImage("heart.png");
}

void draw() {
  background(0);

  if (lives > 0) {
    text(mouseX + ", " + mouseY, 0, 10);

    for (int index = 0; index < existingShapes.size(); index++) {
      existingShapes.get(index).DrawShape();
    }

    for (int index = 0; index < deathParticles.size(); index++) {
      deathParticles.get(index).DrawParticle();
    }

    if (currentShape!=null) {
      currentShape.DrawShape();
    }

    DrawUI();
    DefaultDrawSettings();

    ellipse(mouseX, mouseY, 5, 5);

    if (score >= 5000*level) {
      NextLevel();
    }
  }else{
     text("GAME OVER", width/2, height/2); 
  }
}

void DefaultDrawSettings() {
  fill(255);
  stroke(0);
  textSize(12);
}

PVector GetMouse() {
  return new PVector(mouseX, mouseY);
}

void mousePressed() {
  score+=500;
  if (mouseY > 125) {
    currentShape.PlaceShape();
    PlaceShape();
  }
}



public void NextLevel() {
  level++;
  existingShapes = new ArrayList<Shape>();
  SpawnLevelShapes();
  AddShape();
  globalSpeed+=0.25f;
}


public static float GetSpeed() {
  return globalSpeed;
}
public void PlaceShape() {
  if (!currentShape.followingMouse) {
    existingShapes.add(currentShape);
    AddShape();
  }
}
public void AddShape() {
  currentShape = nextShape;
  nextShape = new Shape((int)random(minShapeSize, maxShapeSize), (int)random(3, 4+level), globalSpeed);  
  globalSpeed+=speedIncrease;
}

public void SpawnLevelShapes() {
  for (int index = 0; index < 3+level; index++) {
    existingShapes.add(new Shape((int)random(75, 125-level*5), (int)random(3, 4+level), globalSpeed, (int)random(100, 900), (int)random(225, 700)));
    while (existingShapes.get(index).boundingBox.CheckOverlap().length > 0) {
      existingShapes.get(index).x = (int)random(100, 900);
      existingShapes.get(index).y = (int)random(225, 575);
      existingShapes.get(index).boundingBox.UpdateBoundingBox(existingShapes.get(index).x, existingShapes.get(index).y);
    }
  }
}

public ArrayList<Shape> GetShapes() {
  return CopyArrayList(existingShapes);
}

public ArrayList<Shape> CopyArrayList(ArrayList<Shape> list) {
  ArrayList<Shape> newList;
  newList = new ArrayList<Shape>();

  for (int index = 0; index < list.size(); index++) {
    newList.add(list.get(index));
  }
  return newList;
}

public void AddDeathParticle(DeathParticle newParticle) {
  deathParticles.add(newParticle);
}
public void RemoveDeathParticle(DeathParticle toRemoveParticle) {
  deathParticles.remove(toRemoveParticle);
}

public void DrawUI() {
  stroke(255);
  fill(0);
  rect(-2, -2, displayWidth+1, 125);
  //line(0,75,displayWidth,75);
  fill(255);
  textSize(26);
  text("Level: " + level, UIBuffer, UIYValue);
  float textW = textWidth("Level: " + level);
  String speedFormatted = nf(globalSpeed, 2, 1);
  text("Speed: " + speedFormatted, UIBuffer + textW + UIBuffer, UIYValue);
  float textW2 = textWidth("Speed: " + speedFormatted);
  String scoreFormatted = nf(score, 6); 
  text("Score: "+scoreFormatted, UIBuffer + textW + UIBuffer + textW2 + UIBuffer, UIYValue);
  float textW3 = textWidth("Score: "+scoreFormatted);
  nextShape.DrawPreview(UIBuffer + textW + UIBuffer + textW2 + UIBuffer + textW3 + UIBuffer + 65, UIYValue-15);

  text("Lives: ", UIBuffer + textW + UIBuffer + textW2 + UIBuffer + textW3 + UIBuffer + 150, UIYValue - 20);
  for (int i = 0; i < lives; i++) {
    image(heart, UIBuffer + textW + UIBuffer + textW2 + UIBuffer + textW3 + UIBuffer + 230 + i * 50, UIYValue - 43);
  }
  text("Allowance: ", UIBuffer + textW + UIBuffer + textW2 + UIBuffer + textW3 + UIBuffer + 150, UIYValue + 20);
}