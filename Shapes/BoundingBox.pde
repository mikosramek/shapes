class BoundingBox {
  public float minX, maxX, minY, maxY;
  float size;
  color boxColour;
  ArrayList<Shape> existingShapes;
  Shape myShape;

  BoundingBox(int size, float x, float y, Shape myShape) {
    this.size = size;
    minX = x - size;
    maxX = x + size;
    minY = y - size;
    maxY = y + size;
    boxColour = color(100, 100, 100);
    this.myShape = myShape;
  }


  public void UpdateBoundingBox(float x, float y) {
    minX = x - size;
    maxX = x + size;
    minY = y - size;
    maxY = y + size;
  }

  public void WeGucci() {
    boxColour = color(100, 100, 100);
  }

  public void DrawBoundingBox() {
    stroke(boxColour);
    line(minX, minY, maxX, minY);
    line(maxX, minY, maxX, maxY);
    line(maxX, maxY, minX, maxY);
    line(minX, maxY, minX, minY);
  }

  public Shape[] CheckOverlap() {
    Shape[] overlappingShapes = new Shape[0];
    int indexForOverlap = 0;
    existingShapes = GetShapes();
    existingShapes.remove(myShape);
    for (int index = 0; index < existingShapes.size(); index++) {
      if (CompareBoxes(existingShapes.get(index).boundingBox)) {
        boxColour = color(100, 0, 0);
        if (existingShapes.get(index).alive) {
          overlappingShapes = Util.AddToArray(overlappingShapes);
          overlappingShapes[indexForOverlap++] = existingShapes.get(index);
        }
      }
    }
    if (overlappingShapes.length == 0) {
      boxColour = color(100);
    }
    //print("\n"+overlappingShapes.length);
    return overlappingShapes;
  }

  private boolean CompareBoxes(BoundingBox otherBox) {
    //ehehehehe ;) 

    //Not overlapping
    if ((maxX < otherBox.minX || minX > otherBox.maxX) || (minY > otherBox.maxY || maxY < otherBox.minY)) {
      return false;
    }

    return true;
  }
}