static class Util {
  public static Shape[] AddToArray(Shape[] array) {
    Shape[] newArray = new Shape[array.length+1];    
    for (int i = 0; i < array.length; i++) {
      newArray[i] = array[i];
    }
    return newArray;
  }

  public static boolean DetectLineCollision(PVector a, PVector b, PVector c, PVector d) {
    float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));
    float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y));
    float numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));

    if (denominator == 0) {
      return numerator1 == 0 && numerator2 == 0;
    }

    float r = numerator1 / denominator;
    float s = numerator2 / denominator;

    return (r >= 0 && r <= 1) && (s >= 0 && s < 1);
  }
  
}