PVector line1A, line1B, line2A, line2B;
float lineLength;
float lineAngle;
color lineColor = color(0, 0, 0);


void setup() {
  lineLength = 0;
  lineAngle = 90;
  size(500, 500);
  line1A = new PVector(0, 0);
  line1B = new PVector(width, height);

  line2A = new PVector(0, 0);
  line2B = new PVector(0, 0);
}

void draw() {
  background(255);
  line(line1A.x, line1A.y, line1B.x, line1B.y);
  stroke(lineColor);
  line(line2A.x, line2A.y, line2B.x, line2B.y);
  UpdateLine2();
  if(DetectLineCollision(line1A, line1B, line2A, line2B)){
    lineColor = color(255,0,0);
  }else{
    lineColor = color(0);
  }
}


void UpdateLine2() {
  line2A.x = mouseX - lineLength;
  line2A.y = mouseY - lineAngle;  
  line2B.x = mouseX + lineLength;
  line2B.y = mouseY + lineAngle;
}

boolean DetectLineCollision(PVector a, PVector b, PVector c, PVector d) {
  float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));
  float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y));
  float numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));

  if (denominator == 0) {
    return numerator1 == 0 && numerator2 == 0;
  }

  float r = numerator1 / denominator;
  float s = numerator2 / denominator;

  return (r >= 0 && r <= 1) && (s >= 0 && s < 1);
}